package com.tradereport;

public class TradeEvent {

	private String symbol;
    private Double price;
    private Double lastPrice;
    private Double percChange;
    private Integer volume;
    
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getLastPrice() {
		return lastPrice;
	}
	public void setLastPrice(Double lastPrice) {
		this.lastPrice = lastPrice;
	}
	public Double getPercChange() {
		return percChange;
	}
	public void setPercChange(Double percChange) {
		this.percChange = percChange;
	}
	public Integer getVolume() {
		return volume;
	}
	public void setVolume(Integer volume) {
		this.volume = volume;
	}
    
}

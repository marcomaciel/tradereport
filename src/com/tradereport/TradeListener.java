package com.tradereport;

import com.bea.wlevs.ede.api.EventRejectedException;
import com.bea.wlevs.ede.api.StreamSink;

public class TradeListener implements StreamSink {

	@Override
	public void onInsertEvent(Object event) throws EventRejectedException {

		if (event instanceof TradeEvent) {
			String symbolProp = ((TradeEvent) event).getSymbol();
			Integer volumeProp = ((TradeEvent) event).getVolume();
			System.out.println(symbolProp + ":" + volumeProp);
		}
	}

}
